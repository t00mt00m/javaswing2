import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;

public class GridBagLayoutExampleMain {

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable()
			{
				public void run() {
					new GridBagLayoutExample();
				}
			});
	}
}

class GridBagLayoutExample {
	public GridBagLayoutExample() {
		JFrame guiFrame = new JFrame();
		
		//make sure the program exits when the frame closes
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("GridBagLayout Example");
		guiFrame.setSize(600,300);
		
		//This will center the JFrame in the middle of the screen
		guiFrame.setLocationRelativeTo(null);
		
		//creating a border to highlight the component areas
		Border outline = BorderFactory.createLineBorder(Color.black);
		
		//create GribBagLayout and the GridBagLayout Constraints
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints cons = new GridBagConstraints();
		
		cons.fill = GridBagConstraints.BOTH;
		
		JPanel compPanel = new JPanel();
		compPanel.setLayout(gridBag);
		
		cons.gridx = 2; // 3  x 3 grid, starts from 0
		cons.gridy = 2;
		JLabel randomLbl = new JLabel("In Xanadu did Kubla Khan, " + "A stately pleasure-dome decree");
		randomLbl.setBorder(outline);
		gridBag.setConstraints(randomLbl, cons);
		compPanel.add(randomLbl);
		
		cons.ipady = 100; //width
		cons.ipadx = 100; //height
		cons.weighty = 1.0;
		cons.gridx = 0; //position in grid
		cons.gridy = 0; 
		JLabel tallLbl = new JLabel("Tall and long");
		tallLbl.setBorder(outline);
		gridBag.setConstraints(tallLbl, cons);
		compPanel.add(tallLbl);
		
		cons.ipady = 100; //width
		cons.ipadx = 50; //height
		cons.weighty = 1.0;
		cons.gridx = 1; //position in grid
		cons.gridy = 0; 
		JLabel tallLbl2 = new JLabel("yuhuu");
		tallLbl2.setBorder(outline);
		gridBag.setConstraints(tallLbl2, cons);
		compPanel.add(tallLbl2);
		
		cons.ipady = 50; 
		cons.ipadx = 100;
		cons.weightx = 0;
		cons.gridx = 0;
		cons.gridy = 1;
		JButton hello = new JButton("Hello");
		gridBag.setConstraints(hello, cons);
		compPanel.add(hello);
		
		cons.ipady = 100;
		cons.ipadx = 10;
		cons.gridx = 1;
		cons.gridy = 1;
		JButton goodbye = new JButton("Goodbye");
		gridBag.setConstraints(goodbye, cons);
		compPanel.add(goodbye);
		
		cons.weightx = 0;
		cons.gridx = 0;
		cons.gridy = 2;
		JButton eh = new JButton("eh");
		gridBag.setConstraints(eh, cons);
		compPanel.add(eh);
		
		guiFrame.add(compPanel);
		guiFrame.setVisible(true);
	}
}







