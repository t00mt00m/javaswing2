import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MusTestV2 extends JFrame {
	MouseListener l = new MouseAdapter();
	MouseMotionListener l2 = new MouseAdapter();
	
	MusTestV2() { // konstruktor
		getContentPane().addMouseListener(l);
		getContentPane().addMouseMotionListener(l2);
		setSize(200,200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	



	
	public static void main(String[] args) {
		MusTestV2 m = new MusTestV2();
	}
 
}

class MouseAdapter implements MouseListener, MouseMotionListener {
	// tom konsturktor
	public void mouseClicked (MouseEvent e) {
		String klick;
		if (e.getClickCount() == 1) klick = "enkel klick";
		else klick = "multipel klick";
		
		String knapp;
		if (SwingUtilities.isLeftMouseButton(e)) knapp = "v�nsterknapp";
		else if (SwingUtilities.isMiddleMouseButton(e)) knapp = "mittenknapp";
		else  knapp = "h�gerknapp";
		System.out.println(klick + " med " + knapp + " vid " + (e.getX() + ", " + e.getY()));
		
	}

	public void mouseEntered(MouseEvent arg0) {
		System.out.println("musen innan");
		
	}

	public void mouseExited(MouseEvent arg0) {
		System.out.println("musen utanf�r");
	}

	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void mouseMoved(MouseEvent e) {
		System.out.println("musen p� " + (e.getX() + ", " + e.getY()));
		
	}


}