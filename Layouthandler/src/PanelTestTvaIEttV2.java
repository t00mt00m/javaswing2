
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelTestTvaIEttV2 {
	public static Component addComponentsToPane (Container p) {
		p.add(new JButton("A"));
		p.add(new JButton("B"));
		p.add(new JButton("C"));
		p.add(new JButton("D"));
		p.add(new JButton("E"));
		p.add(new JButton("�"));
		p.add(new JButton("1"));
		p.add(new JButton("2"));
		
		return p;
	}
	
	public static Component addOtherComponentsToPane (Container p2) {
		p2.add(new JButton("X"));
		p2.add(new JButton("Y"));
		p2.add(new JButton("Z"));
		p2.add(new JButton("�"));
		p2.add(new JButton("�"));
		
		return p2;
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	private static void createAndShowGUI() {
		//Create and setup the window
		JFrame frame = new JFrame("Default FlowLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel masterPanel = new JPanel(new BorderLayout());
		
		JPanel panel1 = new JPanel ();
		addComponentsToPane(panel1);
		
		JPanel panel2 = new JPanel ();
		addOtherComponentsToPane(panel2);
		
		masterPanel.add(panel1, BorderLayout.CENTER);
		masterPanel.add(panel2, BorderLayout.PAGE_END);
		frame.getContentPane().add(masterPanel);
		//Display the window
		
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}





