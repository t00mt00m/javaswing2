import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PekdonFoljning {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI(); 
            }
        });
    }

    private static void createAndShowGUI() {
        JFrame ramverk = new JFrame("Pekdonsf�r�ljare");
        ramverk.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        ramverk.add(new MouseTracker());
       // ramverk.pack();
        ramverk.setSize(400,400);
        ramverk.setVisible(true);
    }

}

class MouseTracker extends JComponent implements MouseListener { 	
	  int mouseX;
	  int mouseY;

	  MouseTracker() {
	    enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
	    // l�gger till en lyssnare
	    addMouseListener(this);
	  }
	  
	  @Override
	  public void paintComponent( Graphics g ) {
	    g.setColor(Color.red);
	    g.drawOval(mouseX-10,mouseY-10,20,20);
	  }

	  @Override
	  public void
	  processMouseMotionEvent( MouseEvent e ) {
	    mouseX = e.getX(); // h�mntar kordinaterna
	    mouseY = e.getY();
// consume() metoden konsumerar eventen "e" s�, att k�llan inte prosesserar det l�ngre.
// Allts�, eventen "e" skickas vidare. 
// Consumes this event so that it will not be processed in the default manner by the source which originated it.
	    e.consume(); 
	    repaint(); // ritar om framen
	  }
	  
	// ===== implementerar lyssnarens interface ======

	  
	  public void mouseClicked(MouseEvent e) {
			System.out.println(mouseX + ", " + mouseY);
			
	  }

	  
	  public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
	  }

	  
	  public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
	  }

	  
	  public void mouseEntered(MouseEvent e) {
		  System.out.println("v�lkommen tillbaka");
	  }

	  
	  public void mouseExited(MouseEvent e) {
			JOptionPane.showMessageDialog(null, "du f�rde musen utanf�r f�nstret");
			
	  }
	}