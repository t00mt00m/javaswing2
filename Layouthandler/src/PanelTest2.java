
import java.awt.Container;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelTest2 {
	public static void addComponentsToPane (Container panel) {
		JPanel p = new JPanel (); // Default layoutmanager is flowlayout
		
		p.add(new JButton("A"));
		p.add(new JButton("B"));
		p.add(new JButton("C"));
		p.add(new JButton("D"));
		p.add(new JButton("E"));
		panel.add(p);
	}
	
	public static void addOtherComponentsToPane (Container panel2) {
		JPanel p2 = new JPanel();
		panel2.setLayout(new BoxLayout (panel2, BoxLayout.X_AXIS)); //setLayout takes a frame inside, not a panel
		p2.add(new JButton("X"));
		p2.add(new JButton("Y"));
		p2.add(new JButton("Z"));
		p2.add(new JButton("�"));
		p2.add(new JButton("�"));
		
		panel2.add(p2);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	private static void createAndShowGUI() {
		JFrame frame = new JFrame("Default FlowLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JFrame frame2 = new JFrame("BoxLayoutDemo");
		
		addComponentsToPane(frame.getContentPane());
		addOtherComponentsToPane(frame2.getContentPane());
		
		frame.setLocation(100, 100); //adds a panel to pos. 100,100 from left upper corner
		
		frame.pack();
		frame.setVisible(true);
		
		frame2.setLocation(250, 250);
		frame2.pack();
		frame2.setVisible(true);
	}
	

}










