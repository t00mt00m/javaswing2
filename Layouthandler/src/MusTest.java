import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MusTest extends JFrame {
	MusTest() { // konstruktor
		getContentPane().addMouseListener(l);
		setSize(200,200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	MouseListener l = new MouseAdapter() {
		@Override
		public void mouseClicked (MouseEvent e) {
			String klick;
			if (e.getClickCount() == 1) klick = "enkel klick";
			else klick = "multipel klick";
			
			String knapp;
			if (SwingUtilities.isLeftMouseButton(e)) knapp = "vänsterknapp";
			else if (SwingUtilities.isMiddleMouseButton(e)) knapp = "mittenknapp";
			else  knapp = "högerknapp";
			System.out.println(klick + " med " + knapp + " vid " + (e.getX() + ", " + e.getY()));
			
		}
	}
	;
	
	public static void main(String[] args) {
		MusTest m = new MusTest();
	}
 
}
