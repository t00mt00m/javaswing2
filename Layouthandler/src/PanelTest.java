import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelTest extends JFrame {
	PanelTest () {
		JPanel p = new JPanel (); // default layoutmanager �r flowlayout
		
		p.add(new JButton("A"));
		p.add(new JButton("B"));
		p.add(new JButton("C"));
		p.add(new JButton("D"));
		p.add(new JButton("E"));
		
		
		add(p);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		
	}
	
	public static void main (String [] args) {
		new PanelTest();
		new AnotherPanel();
	}
}

	class AnotherPanel extends JFrame {
	AnotherPanel() {
		JPanel p2 = new JPanel();
		p2.add(new JButton("x"));
		p2.add(new JButton("y"));
		p2.add(new JButton("z"));
		p2.add(new JButton("�"));

		add(p2);
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}
}